USE [master]
GO
/****** Object:  Database [db_kutuphane]    Script Date: 29.4.2016 19:39:25 ******/
CREATE DATABASE [db_kutuphane]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_kutuphane', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\db_kutuphane.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'db_kutuphane_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\db_kutuphane_log.ldf' , SIZE = 1088KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [db_kutuphane] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_kutuphane].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_kutuphane] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_kutuphane] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_kutuphane] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_kutuphane] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_kutuphane] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_kutuphane] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_kutuphane] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [db_kutuphane] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_kutuphane] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_kutuphane] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_kutuphane] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_kutuphane] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_kutuphane] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_kutuphane] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_kutuphane] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_kutuphane] SET  ENABLE_BROKER 
GO
ALTER DATABASE [db_kutuphane] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_kutuphane] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_kutuphane] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_kutuphane] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_kutuphane] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_kutuphane] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_kutuphane] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_kutuphane] SET RECOVERY FULL 
GO
ALTER DATABASE [db_kutuphane] SET  MULTI_USER 
GO
ALTER DATABASE [db_kutuphane] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_kutuphane] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_kutuphane] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_kutuphane] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'db_kutuphane', N'ON'
GO
USE [db_kutuphane]
GO
/****** Object:  Table [dbo].[tbl_account]    Script Date: 29.4.2016 19:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_account](
	[accountID] [int] IDENTITY(1,1) NOT NULL,
	[account_username] [nvarchar](50) NOT NULL,
	[account_password] [nvarchar](50) NOT NULL,
	[account_name] [nchar](20) NOT NULL,
	[account_lastname] [nchar](20) NOT NULL,
	[account_confirmPassword] [nvarchar](50) NOT NULL,
	[account_email] [nvarchar](70) NOT NULL,
	[isAdmin] [nvarchar](10) NULL,
 CONSTRAINT [PK__tbl_acco__F267253EBFA21791] PRIMARY KEY CLUSTERED 
(
	[accountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_altkategori]    Script Date: 29.4.2016 19:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_altkategori](
	[altkategori_id] [int] IDENTITY(1,1) NOT NULL,
	[altkategori_adi] [varchar](50) NULL,
	[kategorino] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[altkategori_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_kategori]    Script Date: 29.4.2016 19:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_kategori](
	[kategori_id] [int] IDENTITY(1,1) NOT NULL,
	[kategori_adi] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[kategori_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_kitap]    Script Date: 29.4.2016 19:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_kitap](
	[kitap_id] [int] IDENTITY(1,1) NOT NULL,
	[kitap_adi] [varchar](50) NULL,
	[kitap_ozeti] [varchar](max) NULL,
	[kitap_altkategorino] [int] NULL,
	[yazarno] [int] NULL,
	[yayinevino] [int] NULL,
	[kitap_kategorino] [int] NULL,
	[kitap_isbn] [varchar](50) NULL,
	[kitap_resimadres] [varchar](max) NULL,
	[kitap_fiyat] [decimal](10, 2) NULL,
	[kitap_yayintarihi] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[kitap_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_yayinevi]    Script Date: 29.4.2016 19:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_yayinevi](
	[yayinevi_id] [int] IDENTITY(1,1) NOT NULL,
	[yayinevi_adi] [varchar](50) NULL,
	[yayinevi_tel] [varchar](20) NULL,
	[yayinevi_eposta] [varchar](50) NULL,
	[yayinevi_adresi] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[yayinevi_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_yazarlar]    Script Date: 29.4.2016 19:39:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_yazarlar](
	[yazar_id] [int] IDENTITY(1,1) NOT NULL,
	[yazar_adi] [varchar](50) NULL,
	[yazar_soyadi] [varchar](50) NULL,
	[yazar_tel] [varchar](20) NULL,
	[yazar_eposta] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[yazar_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_account] ON 

INSERT [dbo].[tbl_account] ([accountID], [account_username], [account_password], [account_name], [account_lastname], [account_confirmPassword], [account_email], [isAdmin]) VALUES (6, N'murat', N'123', N'murat               ', N'kara                ', N'123', N'muratkara', N'evet')
INSERT [dbo].[tbl_account] ([accountID], [account_username], [account_password], [account_name], [account_lastname], [account_confirmPassword], [account_email], [isAdmin]) VALUES (7, N'ugur', N'123', N'ugur                ', N'abdi                ', N'123', N'ugurabdi', N'hayir')
INSERT [dbo].[tbl_account] ([accountID], [account_username], [account_password], [account_name], [account_lastname], [account_confirmPassword], [account_email], [isAdmin]) VALUES (9, N'öfk', N'123', N'öfk                 ', N'kirli               ', N'123', N'öfk132', N'hayir')
INSERT [dbo].[tbl_account] ([accountID], [account_username], [account_password], [account_name], [account_lastname], [account_confirmPassword], [account_email], [isAdmin]) VALUES (10, N'hasan', N'123', N'hasan               ', N'turgut              ', N'123', N'hasan@turgut.com', N'hayir')
SET IDENTITY_INSERT [dbo].[tbl_account] OFF
SET IDENTITY_INSERT [dbo].[tbl_altkategori] ON 

INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (1, N'ASP.NET', 1)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (2, N'C#', 1)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (3, N'PHP', 1)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (4, N'MSSQL', 1)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (5, N'PCNet', 4)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (6, N'Bilim Teknik', 4)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (7, N'Servay Fizik', 2)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (8, N'Cin Ali', 3)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (11, N'ugur', 1)
INSERT [dbo].[tbl_altkategori] ([altkategori_id], [altkategori_adi], [kategorino]) VALUES (12, N'murat', 5)
SET IDENTITY_INSERT [dbo].[tbl_altkategori] OFF
SET IDENTITY_INSERT [dbo].[tbl_kategori] ON 

INSERT [dbo].[tbl_kategori] ([kategori_id], [kategori_adi]) VALUES (1, N'Bilgisayar/Yazılım')
INSERT [dbo].[tbl_kategori] ([kategori_id], [kategori_adi]) VALUES (2, N'Bilim/Mühendislik')
INSERT [dbo].[tbl_kategori] ([kategori_id], [kategori_adi]) VALUES (3, N'Çocuk Kitapları')
INSERT [dbo].[tbl_kategori] ([kategori_id], [kategori_adi]) VALUES (4, N'Dergiler')
INSERT [dbo].[tbl_kategori] ([kategori_id], [kategori_adi]) VALUES (5, N'Polisiye')
SET IDENTITY_INSERT [dbo].[tbl_kategori] OFF
SET IDENTITY_INSERT [dbo].[tbl_kitap] ON 

INSERT [dbo].[tbl_kitap] ([kitap_id], [kitap_adi], [kitap_ozeti], [kitap_altkategorino], [yazarno], [yayinevino], [kitap_kategorino], [kitap_isbn], [kitap_resimadres], [kitap_fiyat], [kitap_yayintarihi]) VALUES (3, N'ugur', N'ekledi', 123, 123, 123, 3, N'yeni', N'/Image/Screenshot (1).png', CAST(123.00 AS Decimal(10, 2)), N'123')
INSERT [dbo].[tbl_kitap] ([kitap_id], [kitap_adi], [kitap_ozeti], [kitap_altkategorino], [yazarno], [yayinevino], [kitap_kategorino], [kitap_isbn], [kitap_resimadres], [kitap_fiyat], [kitap_yayintarihi]) VALUES (4, N'kitap1', N'özeet', 1, 1, 1, 2, N'123456', N'\Image\Ugur.jpg', CAST(100.00 AS Decimal(10, 2)), N'26.04.2016 14:47:16')
INSERT [dbo].[tbl_kitap] ([kitap_id], [kitap_adi], [kitap_ozeti], [kitap_altkategorino], [yazarno], [yayinevino], [kitap_kategorino], [kitap_isbn], [kitap_resimadres], [kitap_fiyat], [kitap_yayintarihi]) VALUES (5, N'1', N'1', 1, 1, 1, 1, N'1', N'/Image/Ugur.jpg', CAST(1.00 AS Decimal(10, 2)), N'26.04.2016 14:54:07')
INSERT [dbo].[tbl_kitap] ([kitap_id], [kitap_adi], [kitap_ozeti], [kitap_altkategorino], [yazarno], [yayinevino], [kitap_kategorino], [kitap_isbn], [kitap_resimadres], [kitap_fiyat], [kitap_yayintarihi]) VALUES (7, N'123', N'123', 123, 2, 123, 4, N'123', N'/Image/resimyok.png', CAST(123.00 AS Decimal(10, 2)), N'27.04.2016 11:10:53')
INSERT [dbo].[tbl_kitap] ([kitap_id], [kitap_adi], [kitap_ozeti], [kitap_altkategorino], [yazarno], [yayinevino], [kitap_kategorino], [kitap_isbn], [kitap_resimadres], [kitap_fiyat], [kitap_yayintarihi]) VALUES (8, N'1', N'1', 1, 1, 1, 1, N'1', N'/Image/resimyok.png', CAST(1.00 AS Decimal(10, 2)), N'27.4.2016 15:57:55')
INSERT [dbo].[tbl_kitap] ([kitap_id], [kitap_adi], [kitap_ozeti], [kitap_altkategorino], [yazarno], [yayinevino], [kitap_kategorino], [kitap_isbn], [kitap_resimadres], [kitap_fiyat], [kitap_yayintarihi]) VALUES (9, N'1', N'1', 1, 4, 1, 1, N'1', N'/Image/resimyok.png', CAST(1.00 AS Decimal(10, 2)), N'27.4.2016 16:05:59')
INSERT [dbo].[tbl_kitap] ([kitap_id], [kitap_adi], [kitap_ozeti], [kitap_altkategorino], [yazarno], [yayinevino], [kitap_kategorino], [kitap_isbn], [kitap_resimadres], [kitap_fiyat], [kitap_yayintarihi]) VALUES (10, N'turgut', N'reis', 1, 0, 2, 2, N'sd44a4d6a', N'/Image/ugur.jpg', CAST(50000.00 AS Decimal(10, 2)), N'29.4.2016 17:21:24')
INSERT [dbo].[tbl_kitap] ([kitap_id], [kitap_adi], [kitap_ozeti], [kitap_altkategorino], [yazarno], [yayinevino], [kitap_kategorino], [kitap_isbn], [kitap_resimadres], [kitap_fiyat], [kitap_yayintarihi]) VALUES (11, N'berkan', N'cakmak', 8, 6, 1, 3, N'65s45f56sd', N'/Image/ugur.jpg', CAST(54645.00 AS Decimal(10, 2)), N'29.4.2016 17:37:15')
SET IDENTITY_INSERT [dbo].[tbl_kitap] OFF
SET IDENTITY_INSERT [dbo].[tbl_yayinevi] ON 

INSERT [dbo].[tbl_yayinevi] ([yayinevi_id], [yayinevi_adi], [yayinevi_tel], [yayinevi_eposta], [yayinevi_adresi]) VALUES (1, N'Beyaz Balina', N'05555555555', N'beyazbalina@beyazbalina.com', N'beyaz balina caddesi beyaz balina sokak')
INSERT [dbo].[tbl_yayinevi] ([yayinevi_id], [yayinevi_adi], [yayinevi_tel], [yayinevi_eposta], [yayinevi_adresi]) VALUES (2, N'İnkılap Yayınevi', N'054444444444', N'inkilap@inkilap.com', N'inkilap sokak')
SET IDENTITY_INSERT [dbo].[tbl_yayinevi] OFF
SET IDENTITY_INSERT [dbo].[tbl_yazarlar] ON 

INSERT [dbo].[tbl_yazarlar] ([yazar_id], [yazar_adi], [yazar_soyadi], [yazar_tel], [yazar_eposta]) VALUES (1, N'abidik', N'ugur', N'0546516213', N'sda')
INSERT [dbo].[tbl_yazarlar] ([yazar_id], [yazar_adi], [yazar_soyadi], [yazar_tel], [yazar_eposta]) VALUES (2, N'muro', N'asdad', N'asd123', N'adsad')
INSERT [dbo].[tbl_yazarlar] ([yazar_id], [yazar_adi], [yazar_soyadi], [yazar_tel], [yazar_eposta]) VALUES (3, N'ugurabdioglu', NULL, NULL, NULL)
INSERT [dbo].[tbl_yazarlar] ([yazar_id], [yazar_adi], [yazar_soyadi], [yazar_tel], [yazar_eposta]) VALUES (4, N'murat', N'karagozgil', NULL, NULL)
INSERT [dbo].[tbl_yazarlar] ([yazar_id], [yazar_adi], [yazar_soyadi], [yazar_tel], [yazar_eposta]) VALUES (5, N'hasan', N'turgut', NULL, NULL)
INSERT [dbo].[tbl_yazarlar] ([yazar_id], [yazar_adi], [yazar_soyadi], [yazar_tel], [yazar_eposta]) VALUES (6, N'berkan', N'cakmak', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_yazarlar] OFF
ALTER TABLE [dbo].[tbl_account] ADD  CONSTRAINT [DF_tbl_account_isAdmin]  DEFAULT (N'hayir') FOR [isAdmin]
GO
USE [master]
GO
ALTER DATABASE [db_kutuphane] SET  READ_WRITE 
GO
