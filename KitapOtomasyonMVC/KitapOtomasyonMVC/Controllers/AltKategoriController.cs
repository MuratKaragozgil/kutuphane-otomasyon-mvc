﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KitapOtomasyonMVC.Models;
using KitapOtomasyonMVC.DAL;

namespace KitapOtomasyonMVC.Controllers
{
    public class AltKategoriController : Controller
    {
        private KutuphaneContext db = new KutuphaneContext();

        // GET: /AltKategori/
        public ActionResult Index()
        {
            return View(db.tbl_altkategori.ToList());
        }

        // GET: /AltKategori/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_altkategori tbl_altkategori = db.tbl_altkategori.Find(id);
            if (tbl_altkategori == null)
            {
                return HttpNotFound();
            }
            return View(tbl_altkategori);
        }

        // GET: /AltKategori/Create
        public ActionResult Create()
        {
            List<tbl_kategori> kategori = new List<tbl_kategori>();
            kategori = db.tbl_kategori.OrderBy(a => a.kategori_id).ToList();
            ViewBag.kategoriAdi = new SelectList(kategori, "kategori_id", "kategori_adi");

            return View();
        }

        // POST: /AltKategori/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_kategori tbl_kategori ,string alt_kategori_adi)
        {
           

            if (ModelState.IsValid)
            {
                tbl_altkategori alt = new tbl_altkategori();
                alt.altkategori_adi = alt_kategori_adi;
                alt.kategorino = tbl_kategori.kategori_id;
                db.tbl_altkategori.Add(alt);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_kategori);
        }

        // GET: /AltKategori/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_altkategori tbl_altkategori = db.tbl_altkategori.Find(id);
            if (tbl_altkategori == null)
            {
                return HttpNotFound();
            }
            return View(tbl_altkategori);
        }

        // POST: /AltKategori/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="altkategori_id,altkategori_adi,kategorino")] tbl_altkategori tbl_altkategori)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_altkategori).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_altkategori);
        }

        // GET: /AltKategori/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_altkategori tbl_altkategori = db.tbl_altkategori.Find(id);
            if (tbl_altkategori == null)
            {
                return HttpNotFound();
            }
            return View(tbl_altkategori);
        }

        // POST: /AltKategori/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_altkategori tbl_altkategori = db.tbl_altkategori.Find(id);
            db.tbl_altkategori.Remove(tbl_altkategori);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
