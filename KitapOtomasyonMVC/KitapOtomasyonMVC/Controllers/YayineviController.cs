﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KitapOtomasyonMVC.Models;
using KitapOtomasyonMVC.DAL;

namespace KitapOtomasyonMVC.Controllers
{
    public class YayineviController : Controller
    {
        private KutuphaneContext db = new KutuphaneContext();

        // GET: /Yayinevi/
        public ActionResult Index()
        {
            return View(db.tbl_yayinevi.ToList());
        }

        // GET: /Yayinevi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_yayinevi tbl_yayinevi = db.tbl_yayinevi.Find(id);
            if (tbl_yayinevi == null)
            {
                return HttpNotFound();
            }
            return View(tbl_yayinevi);
        }

        // GET: /Yayinevi/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Yayinevi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="yayinevi_id,yayinevi_adi,yayinevi_tel,yayinevi_eposta,yayinevi_adresi")] tbl_yayinevi tbl_yayinevi)
        {
            if (ModelState.IsValid)
            {
                db.tbl_yayinevi.Add(tbl_yayinevi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_yayinevi);
        }

        // GET: /Yayinevi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_yayinevi tbl_yayinevi = db.tbl_yayinevi.Find(id);
            if (tbl_yayinevi == null)
            {
                return HttpNotFound();
            }
            return View(tbl_yayinevi);
        }

        // POST: /Yayinevi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="yayinevi_id,yayinevi_adi,yayinevi_tel,yayinevi_eposta,yayinevi_adresi")] tbl_yayinevi tbl_yayinevi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_yayinevi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_yayinevi);
        }

        // GET: /Yayinevi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_yayinevi tbl_yayinevi = db.tbl_yayinevi.Find(id);
            if (tbl_yayinevi == null)
            {
                return HttpNotFound();
            }
            return View(tbl_yayinevi);
        }

        // POST: /Yayinevi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_yayinevi tbl_yayinevi = db.tbl_yayinevi.Find(id);
            db.tbl_yayinevi.Remove(tbl_yayinevi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
