﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KitapOtomasyonMVC.Models;
using KitapOtomasyonMVC.DAL;
using System.Data.Entity.Validation;

namespace KitapOtomasyonMVC.Controllers
{       //yorum
    public class KullaniciController : Controller
    {
        private AccountContext db = new AccountContext();

        // GET: /Kullanici/
        public ActionResult Index()
        {
            return View(db.tbl_account.ToList());
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(tbl_account account)
        {
            using (AccountContext db = new AccountContext())
            {
                var usr = db.tbl_account.Where(a => a.account_username == account.account_username && a.account_password == account.account_password).FirstOrDefault();
                if (usr != null)
                {
                    Session["username"] = account.account_username;
                    Session["userid"] = account.accountID;

                    Console.WriteLine(usr.isAdmin);

                    if (usr.isAdmin.Equals("evet"))
                    {
                        Session["admin"] = "true";
                        return RedirectToAction("Index", "Kullanici");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Hatali giris yaptiniz !");
                    return View();
                }
            }
        }

        public ActionResult LogOff()
        {
            Session.RemoveAll();
            return RedirectToAction("Login");
        }




        // GET: /Kullanici/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_account tbl_account = db.tbl_account.Find(id);
            if (tbl_account == null)
            {
                return HttpNotFound();
            }
            return View(tbl_account);
        }

        // GET: /Kullanici/Create
        public ActionResult Register()
        {
            return View();
        }

        // POST: /Kullanici/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "accountID,account_username,account_password,account_name,account_lastname,account_confirmPassword,account_email,isAdmin")] tbl_account tbl_account)
        {
            if (ModelState.IsValid)
            {
                db.tbl_account.Add(tbl_account);
                db.SaveChanges();
                return RedirectToAction("Login", "Kullanici");
            }
            return View(tbl_account);
        }
        // GET: /Kullanici/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_account tbl_account = db.tbl_account.Find(id);
            if (tbl_account == null)
            {
                return HttpNotFound();
            }
            return View(tbl_account);
        }

        // POST: /Kullanici/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "accountID,account_username,account_password,account_name,account_lastname,account_confirmPassword,account_email")] tbl_account tbl_account)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_account).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_account);
        }

        // GET: /Kullanici/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_account tbl_account = db.tbl_account.Find(id);
            if (tbl_account == null)
            {
                return HttpNotFound();
            }
            return View(tbl_account);
        }

        // POST: /Kullanici/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_account tbl_account = db.tbl_account.Find(id);
            db.tbl_account.Remove(tbl_account);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
