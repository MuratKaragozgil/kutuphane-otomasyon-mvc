using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KitapOtomasyonMVC.Models;
using KitapOtomasyonMVC.DAL;
using System.IO;

namespace KitapOtomasyonMVC.Controllers
{
    public class KitapController : Controller
    {
        private KutuphaneContext db = new KutuphaneContext();

        // GET: /Kitap/
        public ActionResult Index()
        {
            return View(db.tbl_kitap.ToList());
        }

        // GET: /Kitap/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_kitap tbl_kitap = db.tbl_kitap.Find(id);
            if (tbl_kitap == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kitap);
        }

        // GET: /Kitap/Create
        public ActionResult Create()
        {
            List<tbl_yazarlar> yazar = new List<tbl_yazarlar>();
            yazar = db.tbl_yazarlar.OrderBy(a => a.yazar_id).ToList();
            ViewBag.yazarAdi = new SelectList(yazar, "yazar_id", "yazar_adi");

            List<tbl_yayinevi> yayinEvi = new List<tbl_yayinevi>();
            yayinEvi = db.tbl_yayinevi.OrderBy(a => a.yayinevi_id).ToList();
            ViewBag.yayineviAdi = new SelectList(yayinEvi, "yayinevi_id", "yayinevi_adi");

            List<tbl_kategori> kategori = new List<tbl_kategori>();
            kategori = db.tbl_kategori.OrderBy(a => a.kategori_id).ToList();
            ViewBag.kategoriAdi = new SelectList(kategori, "kategori_id", "kategori_adi");

            List<tbl_altkategori> altkategori = new List<tbl_altkategori>();
            altkategori = db.tbl_altkategori.OrderBy(a => a.altkategori_id).ToList();
            ViewBag.altkategoriAdi = new SelectList(altkategori, "altkategori_id", "altkategori_adi");

            return View();
        }

        // POST: /Kitap/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "kitap_id,kitap_adi,kitap_ozeti,kitap_altkategorino,yazarno,yayinevino,kitap_kategorino,kitap_isbn,kitap_resimadres,kitap_fiyat,kitap_yayintarihi")] 
            tbl_kitap tbl_kitap, HttpPostedFileBase file,string yazar_adi,string yazarsoyadi)
        {
            if (ModelState.IsValid)
            {
                string path = null;
                string pic = "resimyok.png";
                if (file != null)
                {
                    pic = System.IO.Path.GetFileName(file.FileName);
                    path = System.IO.Path.Combine(
                    Server.MapPath("~/Image/"), pic);
                    // file is uploaded
                    file.SaveAs(path);
                }
                
                tbl_yazarlar tbl = new tbl_yazarlar();
                tbl.yazar_adi = yazar_adi;
                tbl.yazar_soyadi = yazarsoyadi;
                tbl_kitap.kitap_resimadres = "/Image/" + pic;
                tbl_kitap.kitap_yayintarihi = DateTime.Now.ToString();
                tbl_kitap.yazarno = db.tbl_yazarlar.Max(a => a.yazar_id)+1;
                db.tbl_kitap.Add(tbl_kitap);
                db.tbl_yazarlar.Add(tbl);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_kitap);
        }

        // GET: /Kitap/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_kitap tbl_kitap = db.tbl_kitap.Find(id);
            if (tbl_kitap == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kitap);
        }

        // POST: /Kitap/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "kitap_id,kitap_adi,kitap_ozeti,kitap_altkategorino,yazarno,yayinevino,kitap_kategorino,kitap_isbn,kitap_resimadres,kitap_fiyat,kitap_yayintarihi")] tbl_kitap tbl_kitap)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_kitap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_kitap);
        }

        // GET: /Kitap/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_kitap tbl_kitap = db.tbl_kitap.Find(id);
            if (tbl_kitap == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kitap);
        }

        // POST: /Kitap/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_kitap tbl_kitap = db.tbl_kitap.Find(id);
            db.tbl_kitap.Remove(tbl_kitap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult UgurIndex()
        {

            var join = (from kitap in db.tbl_kitap
                        join yazar in db.tbl_yazarlar
                        on kitap.yazarno equals yazar.yazar_id
                        select new
                        {
                            kitap_adi = kitap.kitap_adi,
                            kitap_altkategoriNo = kitap.kitap_altkategorino,
                            kitap_fiyat = kitap.kitap_fiyat,
                            kitap_id = kitap.kitap_id,
                            kitap_isbn = kitap.kitap_isbn,
                            kitap_kategorino = kitap.kitap_kategorino,
                            kitap_ozeti = kitap.kitap_ozeti,
                            kitap_resimadres = kitap.kitap_resimadres,
                            kitap_yayintarihi = kitap.kitap_yayintarihi,

                            yazar_adi = yazar.yazar_adi,
                            yazar_soyadi = yazar.yazar_soyadi,
                            yazar_tel = yazar.yazar_tel,
                            yazar_eposta = yazar.yazar_eposta,
                        }).ToList();

            return View(db.tbl_kitap.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult hepsi()
        {
            var kitaplar = db.tbl_kitap.ToList();
            var yazarlar = db.tbl_yazarlar.ToList();
            var view = new KitapOtomasyonMVC.Models.hepsi
            {
                hepsi_tbl_yazarlar = yazarlar,
                hepsi_tbl_kitap = kitaplar

            };

            return View(view);
        }
    }
}