﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KitapOtomasyonMVC.DAL;

namespace KitapOtomasyonMVC.Controllers
{
    public class HomeController : Controller
    {
        private KutuphaneContext db = new KutuphaneContext();

        
        public ActionResult Index()
        {
            int kategori_id = Convert.ToInt32(this.Request.QueryString["kate"]);
            int alt_kategori_id = Convert.ToInt32(this.Request.QueryString["altkate"]);

            var kitaplar = db.tbl_kitap.ToList();

            if (kategori_id != 0)
            {
                kitaplar = db.tbl_kitap.Where(a => a.kitap_kategorino == kategori_id).ToList();
            }
            if (alt_kategori_id != 0)
            {

                kitaplar = db.tbl_kitap.Where(a => a.kitap_altkategorino == alt_kategori_id).ToList();
            }

            var yazarlar = db.tbl_yazarlar.ToList();
            var kategoriler = db.tbl_kategori.ToList();
            var altkategoriler = db.tbl_altkategori.ToList();
            var view = new KitapOtomasyonMVC.Models.hepsi
            {
                hepsi_tbl_yazarlar = yazarlar,
                hepsi_tbl_kitap = kitaplar,
                hepsi_tbl_kategori = kategoriler,
                hepsi_tbl_altkategori = altkategoriler

            };

            return View(view);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}