﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KitapOtomasyonMVC.Models;
using KitapOtomasyonMVC.DAL;

namespace KitapOtomasyonMVC.Controllers
{
    public class YazarController : Controller
    {
        private KutuphaneContext db = new KutuphaneContext();

        // GET: /Yazar/
        public ActionResult Index()
        {
            return View(db.tbl_yazarlar.ToList());
        }

        // GET: /Yazar/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_yazarlar tbl_yazarlar = db.tbl_yazarlar.Find(id);
            if (tbl_yazarlar == null)
            {
                return HttpNotFound();
            }
            return View(tbl_yazarlar);
        }

        // GET: /Yazar/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Yazar/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="yazar_id,yazar_adi,yazar_soyadi,yazar_tel,yazar_eposta")] tbl_yazarlar tbl_yazarlar)
        {
            if (ModelState.IsValid)
            {
                db.tbl_yazarlar.Add(tbl_yazarlar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_yazarlar);
        }

        // GET: /Yazar/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_yazarlar tbl_yazarlar = db.tbl_yazarlar.Find(id);
            if (tbl_yazarlar == null)
            {
                return HttpNotFound();
            }
            return View(tbl_yazarlar);
        }

        // POST: /Yazar/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="yazar_id,yazar_adi,yazar_soyadi,yazar_tel,yazar_eposta")] tbl_yazarlar tbl_yazarlar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_yazarlar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_yazarlar);
        }

        // GET: /Yazar/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_yazarlar tbl_yazarlar = db.tbl_yazarlar.Find(id);
            if (tbl_yazarlar == null)
            {
                return HttpNotFound();
            }
            return View(tbl_yazarlar);
        }

        // POST: /Yazar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_yazarlar tbl_yazarlar = db.tbl_yazarlar.Find(id);
            db.tbl_yazarlar.Remove(tbl_yazarlar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
