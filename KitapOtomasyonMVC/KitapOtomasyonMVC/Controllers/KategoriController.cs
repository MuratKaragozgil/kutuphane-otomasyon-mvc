﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KitapOtomasyonMVC.Models;
using KitapOtomasyonMVC.DAL;

namespace KitapOtomasyonMVC.Controllers
{
    public class KategoriController : Controller
    {
        private KutuphaneContext db = new KutuphaneContext();

        // GET: /Kategori/
        public ActionResult Index()
        {
            return View(db.tbl_kategori.ToList());
        }

        // GET: /Kategori/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_kategori tbl_kategori = db.tbl_kategori.Find(id);
            if (tbl_kategori == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kategori);
        }

        // GET: /Kategori/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Kategori/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="kategori_id,kategori_adi")] tbl_kategori tbl_kategori)
        {
            if (ModelState.IsValid)
            {
                db.tbl_kategori.Add(tbl_kategori);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_kategori);
        }

        // GET: /Kategori/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_kategori tbl_kategori = db.tbl_kategori.Find(id);
            if (tbl_kategori == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kategori);
        }

        // POST: /Kategori/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="kategori_id,kategori_adi")] tbl_kategori tbl_kategori)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_kategori).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_kategori);
        }

        // GET: /Kategori/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_kategori tbl_kategori = db.tbl_kategori.Find(id);
            if (tbl_kategori == null)
            {
                return HttpNotFound();
            }
            return View(tbl_kategori);
        }

        // POST: /Kategori/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_kategori tbl_kategori = db.tbl_kategori.Find(id);
            db.tbl_kategori.Remove(tbl_kategori);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
