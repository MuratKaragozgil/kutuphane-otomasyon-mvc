﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KitapOtomasyonMVC.Startup))]
namespace KitapOtomasyonMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
