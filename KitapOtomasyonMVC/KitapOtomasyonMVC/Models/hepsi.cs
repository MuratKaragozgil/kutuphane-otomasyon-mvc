﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KitapOtomasyonMVC.Models
{
    public class hepsi
    {
        public List<tbl_kitap> hepsi_tbl_kitap { get; set; }
        public List<tbl_yazarlar> hepsi_tbl_yazarlar { get; set; }
        public List<tbl_kategori> hepsi_tbl_kategori { get; set; }
        public List<tbl_altkategori> hepsi_tbl_altkategori { get; set; }

    }
}