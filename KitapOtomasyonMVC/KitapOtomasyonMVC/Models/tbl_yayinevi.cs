//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KitapOtomasyonMVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    
    public partial class tbl_yayinevi
    {   
        public int yayinevi_id { get; set; }
        [DisplayName("Yay�nevi Ad� :")]
        [Required(ErrorMessage = "Yay�nevi ad� Bo� Ge�ilemez")]
        public string yayinevi_adi { get; set; }
        [DisplayName("Yay�nevi Tel :")]
        [Required(ErrorMessage = "Yay�nevi tel Bo� Ge�ilemez")]
        public string yayinevi_tel { get; set; }
        [DisplayName("Yay�nevi E-posta :")]
        [Required(ErrorMessage = "Yay�nevi e-posta Bo� Ge�ilemez")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "L�tfen Formata Uygun Bir Mail Adresi Giriniz! �rnek: example123@example123.com")]
        public string yayinevi_eposta { get; set; }
        [DisplayName("Yay�nevi Adresi : ")]
        public string yayinevi_adresi { get; set; }
    }
}
