﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace KitapOtomasyonMVC.DAL
{
    public class AccountContext : DbContext
    {
        public AccountContext() : base("db_kutuphaneEntities") { }

        [Key]
        public System.Data.Entity.DbSet<KitapOtomasyonMVC.Models.tbl_account> tbl_account { get; set; }
    }
}