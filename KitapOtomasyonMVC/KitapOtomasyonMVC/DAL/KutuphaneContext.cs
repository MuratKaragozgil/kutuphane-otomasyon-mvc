﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace KitapOtomasyonMVC.DAL
{
    public class KutuphaneContext : DbContext
    {
        public KutuphaneContext() : base("db_kutuphaneEntities") { }

        public System.Data.Entity.DbSet<KitapOtomasyonMVC.Models.tbl_kitap> tbl_kitap { get; set; }
        public System.Data.Entity.DbSet<KitapOtomasyonMVC.Models.tbl_yazarlar> tbl_yazarlar { get; set; }
        public System.Data.Entity.DbSet<KitapOtomasyonMVC.Models.tbl_kategori> tbl_kategori { get; set; }
        public System.Data.Entity.DbSet<KitapOtomasyonMVC.Models.tbl_yayinevi> tbl_yayinevi { get; set; }
        public System.Data.Entity.DbSet<KitapOtomasyonMVC.Models.tbl_altkategori> tbl_altkategori { get; set; }
        
    }
}